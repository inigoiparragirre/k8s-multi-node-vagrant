# Installation notes


## Vagrant
Use `vagrant up` for machine creation and setup.


## Nodes setup with kubeadm

- First, `vagrant ssh` to Master machine node.
- Select CNI provider (for example Calico)
- Init master node with kubeadm: 
    `kubeadm init --apiserver-advertise-addres=192.168.205.10 --pod-network-cidr=192.168.0.0/16`

- Update yaml for CNI provider (Calico example): 

    ```
    kubectl apply -f https://docs.projectcalico.org/v3.3/getting-started/kubernetes/installation/hosted/rbac-kdd.yaml
    kubectl apply -f https://docs.projectcalico.org/v3.3/getting-started/kubernetes/installation/hosted/kubernetes-datastore/ calico-networking/1.7/calico.yaml
    ```

- Copy config file from /etc/kubernetes/admin.conf to $USER/.kube/config:

    ```
    mkdir -p $HOME/.kube
    sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config 
    sudo chown $(id -u):$(id -g) $HOME/.kube/config
    ```

- After, `vagrant ssh` to Worker machine node
- Join worker node with kubeadm with token:
 
    ```
    kubeadm join --discovery-token abcdef.1234567890abcdef --discovery-token-ca-cert-hash sha256:1234..cdef 192.168.205.10:6443
    ```
- For new token creation use `kubeadm token create --print-join-command ` in master node.
- Use scp or vagrant-scp from master node to host machine to communicate to the cluster:

    `scp -r vagrant@192.168.205.10:/home/vagrant/.kube .`
